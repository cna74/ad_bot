from khayyam import JalaliDatetime
from datetime import datetime
import sqlite3
import pytz
import time

db_connect = sqlite3.connect(database='bot_db.db', check_same_thread=False)
cursor = db_connect.cursor()


def current_time() -> tuple:
    utc = pytz.utc
    u = time.gmtime()
    utc_dt = datetime(u[0], u[1], u[2], u[3], u[4], u[5], tzinfo=utc)
    eastern = pytz.timezone('Asia/Tehran')
    loc_dt = utc_dt.astimezone(eastern)
    return JalaliDatetime().now().strftime('%Y-%m-%d'), loc_dt.strftime('%H%M%S')


def create():
    try:
        db_connect.execute("CREATE TABLE IF NOT EXISTS user("
                           "ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
                           "user_id INTEGER,"
                           "f_name TEXT,"
                           "l_name TEXT,"
                           "file_id TEXT DEFAULT NULL,"
                           "caption TEXT DEFAULT NULL,"
                           "kind TEXT DEFAULT NULL,"
                           "form TEXT DEFAULT NULL,"
                           "logo_id TEXT DEFAULT NULL,"
                           "last_time TEXT DEFAULT NULL,"
                           "times INTEGER DEFAULT 0);")
        db_connect.commit()
    except Exception as E:
        print(E)


def add_user(user_id, f_name, l_name):
    try:
        id_s = [i[0] for i in db_connect.execute('SELECT user_id FROM user').fetchall()]
        if user_id not in id_s:
            db_connect.execute("INSERT INTO user(user_id, f_name, l_name) VALUES(?,?,?)", (user_id, f_name, l_name))
            db_connect.commit()
    except Exception as E:
        print(E)


def add_file_id_kind_caption(file_id, kind, user_id, form=None, caption=None):
    try:
        db_connect.execute("UPDATE user SET file_id=?, caption=?, kind=?, form=? WHERE user_id=?",
                           (file_id, caption, kind, form, user_id))
        db_connect.commit()
    except Exception as E:
        print(E)


def add_logo_id(logo_id: str, user_id):
    try:
        db_connect.execute("UPDATE user SET logo_id=? WHERE user_id=?", (logo_id, user_id))
        db_connect.commit()
    except Exception as E:
        print(E)


def get_members():
    try:
        members = db_connect.execute("SELECT f_name, l_name, times FROM user").fetchall()
        return members
    except Exception as E:
        print(2002, E)


def get_times(user_id):
    try:
        times = db_connect.execute("SELECT times FROM user WHERE user_id=?", (user_id,)).fetchone()[0]
        if times:
            return times
        else:
            return 0
    except Exception as E:
        print(E)


def get_file_id(user_id: str) -> tuple:
    try:
        ret = db_connect.execute("SELECT file_id, logo_id, kind, form, caption FROM user WHERE user_id=?",
                                 (user_id,)).fetchall()[0]
        return ret
    except Exception as E:
        print(1001, E)


def get_logo_id(user_id):
    try:
        ret = db_connect.execute("SELECT logo_id FROM user WHERE user_id=?", (user_id,)).fetchone()[0]
        return ret
    except Exception as E:
        print(E)


def job_finisher(user_id: int):
    try:
        last_time = '-'.join(current_time())
        times = int(db_connect.execute("SELECT times FROM user").fetchone()[0])
        times += 1

        db_connect.execute('UPDATE user SET last_time=?, times=? WHERE user_id=?', (last_time, times, user_id))
        db_connect.commit()
    except Exception as E:
        print(E)
