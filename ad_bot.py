from telegram.ext import (Updater, MessageHandler, CommandHandler,
                          Filters, ConversationHandler, RegexHandler, CallbackQueryHandler)
from moviepy.editor import VideoFileClip, ImageClip, CompositeVideoClip
from telegram import ReplyKeyboardMarkup, InlineKeyboardMarkup, InlineKeyboardButton as Inline
from database import *
from pprint import pprint
from PIL import Image
import telegram
import os

robot = telegram.Bot("588840229:AAGWStr7kJ7ag_jrECbUnhpL4FGbTE2AIDQ")
updater = Updater("588840229:AAGWStr7kJ7ag_jrECbUnhpL4FGbTE2AIDQ")
channel = '@CrazyMind3'
sina = 103086461
create()


def vid_watermark(gif, form, name, logo_id=None, position=None, save_in=None):
    try:
        logo_name = 'CC'
        div = 4.5
        if save_in == 'g':
            save_in = 'gif'
        elif save_in == 'v':
            save_in = 'vid'
        if not position:
            position = 7
        if logo_id:
            if logo_id in ('CC1', 'CC2', 'CC3', 'CC4'):
                logo_name = logo_id
            else:
                logo_name = name
                robot.get_file(logo_id).download('logo/{}.png'.format(logo_name))

        robot.getFile(gif).download('gif/{}.{}'.format(name, form))
        file = 'gif/{}.{}'.format(name, form)
        pos = {1: ('left', 'top'), 2: ('center', 'top'), 3: ('right', 'top'),
               4: ('left', 'center'), 5: ('center', 'center'), 6: ('right', 'center'),
               7: ('left', 'bottom'), 8: ('center', 'bottom'), 9: ('right', 'bottom')}
        clip = VideoFileClip(file)
        w, h = clip.size

        logo = ImageClip("logo/{}.png".format(logo_name)) \
            .set_duration(clip.duration) \
            .resize(width=w // div, height=h // div) \
            .set_pos(pos.get(position, 7))
        final = CompositeVideoClip([clip, logo])
        final.write_videofile(filename='{}/{}.{}'.format(save_in, name, form),
                              verbose=False, progress_bar=False)
    except Exception as E:
        print(E)


def image_watermark(photo, name, logo_id=None, position=None):
    try:
        logo_name = 'CC'
        div = 4.5
        if not position:
            position = 7
        if logo_id:
            if logo_id in ('CC1', 'CC2', 'CC3', 'CC4'):
                logo_name = logo_id
            else:
                logo_name = name
                robot.get_file(logo_id).download('logo/{}.png'.format(logo_name))

        robot.getFile(photo).download('image/{}.jpg'.format(name))
        bg = Image.open('image/{}.jpg'.format(name))
        lg = Image.open('logo/{}.png'.format(logo_name)).convert("RGBA")
        res, lg_sz = bg.size, lg.size
        if res[0] > res[1]:
            n_def = (res[1] // div, res[1] // div)
        else:
            n_def = (res[0] // div, res[0] // div)

        lg.thumbnail(n_def, Image.ANTIALIAS)
        lg_sz = lg.size
        dict1 = {1: (0, 0),
                 2: ((res[0] // 2) - (lg_sz[0] // 2), 0),
                 3: (res[0] - lg_sz[0], 0),
                 4: (0, (res[1] // 2 - lg_sz[1] // 2)),
                 5: (res[0] // 2 - lg_sz[0] // 2, res[1] // 2),
                 6: (res[0] - lg_sz[0], res[1] // 2 - lg_sz[1] // 2),
                 7: (0, res[1] - lg_sz[1]),
                 8: (res[0] // 2 - lg_sz[0] // 2, res[1] - lg_sz[1]),
                 9: (res[0] - lg_sz[0], res[1] - lg_sz[1])}

        bg.paste(lg, dict1.get(position, 7), lg)
        bg.save('image/{}.jpg'.format(name))

    except Exception as E:
        print(E)


def member(bot, update, args):
    try:
        mem = get_members()
        if args:
            if args[0] == 'c':
                bot.send_message(chat_id=sina,
                                 text=str(len(mem)))
            elif args[0] == 'fl':
                fl = [' '.join(i[:1]) for i in mem]
                x = ' '.join(fl)
                bot.send_message(chat_id=sina,
                                 text=x)
        else:
            full = [' '.join([str(j) for j in i])+'\n' for i in mem]
            x = ' '.join(full)
            bot.send_message(chat_id=sina,
                             text=x)
    except Exception as E:
        print(1001, E)


def get_image_gif_video(bot, update):
    try:
        um = update.message
        if um.chat.type == 'private':
            add_user(um.from_user.id,
                     um.from_user.first_name,
                     um.from_user.last_name)
            times = get_times(um.from_user.id)
            if times < 10 or robot.get_chat_member(channel, um.from_user.id):
                if um.photo:
                    add_file_id_kind_caption(file_id=um.photo[-1].file_id,
                                             kind='p',
                                             user_id=um.from_user.id,
                                             caption=um.caption)
                elif um.document:
                    if um.document.mime_type in ('video/mp4', 'image/gif') and um.document.file_size / 1000000 < 10:
                        form = um.document.mime_type.split('/')[1]
                        add_file_id_kind_caption(file_id=um.document.file_id,
                                                 kind='g',
                                                 user_id=um.from_user.id,
                                                 form=form,
                                                 caption=um.caption)
                elif um.video:
                    print(um.video.mime_type)
                    if um.video.mime_type in ('video/mp4', 'video/avi') and um.video.file_size / 1000000 < 10:
                        form = um.video.mime_type.split('/')[1]
                        add_file_id_kind_caption(file_id=um.video.file_id,
                                                 kind='v',
                                                 user_id=um.from_user.id,
                                                 form=form,
                                                 caption=um.caption)
            else:
                bot.send_message(chat_id=um.chat_id,
                                 text="به سقف استفاده از ربات رسیده اید\n"
                                      "برای استفاده نامحدود عضو کانال حامی ربات شوید\n"
                                      "{}".format(channel))
                return ConversationHandler.END
        else:
            bot.send_message("ربات قابلیت پاسخگویی در گروه ها را ندارد!!\n"
                             "{}".format(channel))
            return ConversationHandler.END

        bot.send_photo(chat_id=update.message.chat_id,
                       photo=open('logo/info.png', 'rb'),
                       caption='خب الان لوگویی که میخوای بزاری روی عکس رو بصورت فایل بفرست بهتره که فرمتش png باشه '
                               'اگه نداری یکی از اینا رو انتخاب کن 👆',
                       reply_markup=InlineKeyboardMarkup([
                                                        [Inline('🖼', callback_data='CC1'), Inline('🖼', callback_data='CC2')],
                                                        [Inline('🖼', callback_data='CC3'), Inline('🖼', callback_data='CC4')],
                                                        [Inline('/cancel', callback_data='cancel')]]),
                       reply_to_message_id=um.message_id,
                       one_time_keyboard=True, resize_keyboard=True)
        return get_logo
    except Exception as E:
        print(E)


def get_logo(bot, update):
    try:
        chat_id = None
        if update.message:
            um = update.message
            chat_id = um.chat_id
            if um.document:
                if um.document.mime_type == 'image/png' and um.document.file_size/1000000 < 10:
                    add_logo_id(um.document.file_id, um.from_user.id)
            elif um.photo:
                add_logo_id(um.photo[-1].file_id, um.from_user.id)
        elif update.callback_query:
            u = update.callback_query
            chat_id = u.message.chat_id
            if u.data == 'cancel':
                bot.send_message(chat_id=chat_id,
                                 text="پیش ما بیا\n"
                                 "{}".format(channel))
                return ConversationHandler.END
            else:
                add_logo_id(u.data, u.from_user.id)
        reply_markup = InlineKeyboardMarkup([
            [Inline('1', callback_data='1'), Inline('2', callback_data='2'), Inline('3', callback_data='3')],
            [Inline('4', callback_data='4'), Inline('5', callback_data='5'), Inline('6', callback_data='6')],
            [Inline('7', callback_data='7'), Inline('8', callback_data='8'), Inline('9', callback_data='9')],
        ])
        bot.send_photo(chat_id=chat_id,
                       photo=open('info.png', 'rb'),
                       caption="میخوای لوگو رو کجای تصویر قرار بدی؟",
                       reply_to_message_id=u.message.message_id,
                       reply_markup=reply_markup)
        return out
    except Exception as E:
        print(E)


def out(bot, update):
    try:
        chat_id = update.callback_query.message.chat_id
        um = update.callback_query
        user_id = um.from_user.id
        position = int(um.data)

        pm = bot.send_message(text='درحال بارگذاری ..', chat_id=chat_id)
        background, logo, kind, form, caption = get_file_id(user_id=user_id)
        bot.edit_message_text(chat_id=chat_id, text='درحال بارگذاری ....', message_id=pm.message_id)
        logo = None if logo == 'skip' else logo

        if kind == 'p':
            image_watermark(photo=background, name=user_id, logo_id=logo, position=position)
            bot.edit_message_text(chat_id=chat_id, text='تمام', message_id=pm.message_id)
            bot.send_chat_action(chat_id=chat_id, action=telegram.ChatAction.UPLOAD_PHOTO)
            bot.send_photo(chat_id=chat_id, photo=open('image/{}.jpg'.format(user_id), 'rb'), caption=caption)
            os.remove('image/{}.jpg'.format(user_id))
        elif kind in ('g', 'v'):
            vid_watermark(gif=background, name=user_id, form=form, logo_id=logo, position=position, save_in=kind)
            bot.edit_message_text(chat_id=chat_id, text='تمام', message_id=pm.message_id)
            bot.send_chat_action(chat_id=chat_id, action=telegram.ChatAction.UPLOAD_VIDEO)
            if kind == 'g':
                bot.send_document(chat_id=chat_id, document=open('gif/{}.mp4'.format(user_id), 'rb'), caption=caption)
                os.remove('gif/{}.mp4'.format(user_id))
            elif kind == 'v':
                bot.send_video(chat_id=chat_id, video=open('vid/{}.mp4'.format(user_id), 'rb'), caption=caption)
                os.remove('vid/{}.mp4'.format(user_id))
        job_finisher(user_id)
        return ConversationHandler.END
    except Exception as E:
        print(2002, E)


def cancel(bot, update):
    bot.send_message(chat_id=update.message.chat_id,
                     text='پیش ما بیا\n'
                          '{}'.format(channel),
                     reply_to_message_id=update.message.message_id)
    return ConversationHandler.END


# region start
updater.start_polling()
dpa = updater.dispatcher.add_handler
dpa(CommandHandler(command='member', callback=member, filters=Filters.user((sina,)), pass_args=True))
dpa(ConversationHandler(entry_points=[MessageHandler(Filters.photo, get_image_gif_video),
                                      MessageHandler(Filters.video, get_image_gif_video),
                                      MessageHandler(Filters.document, get_image_gif_video)],
                        states={
                            get_logo: [CallbackQueryHandler(get_logo),
                                       MessageHandler(Filters.document, get_logo),
                                       MessageHandler(Filters.photo, get_logo),
                                       MessageHandler(Filters.text, get_logo)],

                            out: [CallbackQueryHandler(out)]
                        },
                        fallbacks=[CommandHandler('cancel', cancel)]))
updater.idle()
# endregion
